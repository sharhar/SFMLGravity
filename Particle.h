#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

class Particle {
private:
	sf::Vector2f m_Pos;
	sf::Vector2f m_Velocity;
public:
	Particle(sf::Vector2f pos);

	void update();
	void render();
};