#include "Window.h"

ShrimpWindow::ShrimpWindow(int width, int height, std::string title) {
	m_Window = new sf::RenderWindow(sf::VideoMode(width, height), title);

	glClearColor(0, 0, 0, 1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, m_Window->getSize().x, 0, m_Window->getSize().y, -1, 1);
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_TEXTURE_2D);

	m_Window->setFramerateLimit(60);
}

void ShrimpWindow::update() {
	m_Window->display();
}

void ShrimpWindow::prepare() {
	glClear(GL_COLOR_BUFFER_BIT);

        sf::Event event;
        while (m_Window->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                m_Window->close();
        }
}
bool ShrimpWindow::isOpen() {
	return m_Window->isOpen();
}

sf::RenderWindow* ShrimpWindow::getWindow() const {
	return m_Window;
}

sf::Vector2i* ShrimpWindow::getMousePos() const {
	return m_MousePos;
}