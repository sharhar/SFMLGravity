#pragma once

#include <string>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

class ShrimpWindow {
private:
	sf::RenderWindow* m_Window;
	sf::Vector2i* m_MousePos;
public:
	ShrimpWindow(int width, int height, std::string title);

	void update();
	void prepare();
	bool isOpen();

	sf::RenderWindow* getWindow() const;
	sf::Vector2i* getMousePos() const;
};