#include "Particle.h"
#include "shrimp/Window.h"

#include <iostream>

using namespace std;

int main()
{
    ShrimpWindow window(800,600,"Window");

    while (window.isOpen())
    {
    	window.prepare();

    	window.update();
    }

    return 0;
}
